package com.immersivereactors.immersivenuclear;

import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.immersivereactors.immersivenuclear.common.INContent;
import com.immersivereactors.immersivenuclear.common.INTileTypes;
import com.immersivereactors.immersivenuclear.common.Block.Blocks;
import com.immersivereactors.immersivenuclear.common.Items.Items;

// The value here should match an entry in the META-INF/mods.toml file
@Mod(ImmersiveNuclear.MOD_ID)
public class ImmersiveNuclear
{
    public static final String MOD_ID = "immersivenuclear";
    // Directly reference a log4j logger.
    private static final Logger LOGGER = LogManager.getLogger();

    public ImmersiveNuclear() {
    	LOGGER.log(Level.DEBUG,"Starting....");
    	IEventBus eventbus = FMLJavaModLoadingContext.get().getModEventBus();
    	
    	Items.register(eventbus);
    	Blocks.register(eventbus);
    	LOGGER.log(Level.DEBUG,"Constructing....");
    	INContent.population();
    	LOGGER.log(Level.DEBUG,"Initializing....");
    	INContent.initialization();
    	LOGGER.log(Level.DEBUG,"Registering....");
    	INTileTypes.REGISTER.register(FMLJavaModLoadingContext.get().getModEventBus());
    	LOGGER.log(Level.DEBUG,"Registered");
    	//eventbus.addListener(this::setup);
        // Register ourselves for server and other game events we are interested in
        MinecraftForge.EVENT_BUS.register(this);
    }
}
