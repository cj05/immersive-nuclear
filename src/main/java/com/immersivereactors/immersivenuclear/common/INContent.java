package com.immersivereactors.immersivenuclear.common;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;

import com.immersivereactors.immersivenuclear.common.Block.Blocks.Multiblock;
import com.immersivereactors.immersivenuclear.common.Block.metal.*;
import com.immersivereactors.immersivenuclear.common.Block.multiblock.BWRMultiblock;
import com.immersivereactors.immersivenuclear.ImmersiveNuclear;

import net.minecraft.block.Block;
import net.minecraft.fluid.Fluid;
import net.minecraft.item.Item;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus;
import blusunrize.immersiveengineering.api.multiblocks.MultiblockHandler;
import blusunrize.immersiveengineering.common.blocks.metal.MetalMultiblockBlock;

@Mod.EventBusSubscriber(modid = ImmersiveNuclear.MOD_ID, bus = Bus.MOD)
public class INContent {
	public static final List<Block> registeredBlocks = new ArrayList<>();
	public static final List<Item> registeredItems = new ArrayList<>();
	public static final List<Fluid> registeredFluids = new ArrayList<>();
	public static void population() {
		Multiblock.BWReactor = new BWRBlock();
	}
	public static void initialization() {
		MultiblockHandler.registerMultiblock(BWRMultiblock.self);
	}
}
