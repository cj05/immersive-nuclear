package com.immersivereactors.immersivenuclear.common.Block.metal;

import java.util.Set;
import java.util.function.Supplier;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import blusunrize.immersiveengineering.api.crafting.MultiblockRecipe;
import blusunrize.immersiveengineering.common.blocks.generic.PoweredMultiblockTileEntity;
import blusunrize.immersiveengineering.common.blocks.IETileProviderBlock;
import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.material.MaterialColor;
import net.minecraft.block.material.PushReaction;
//import blusunrize.immersiveengineering.common.blocks.multiblocks.IETemplateMultiblock;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntityType;
//import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.Direction;
import net.minecraft.util.NonNullList;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.common.ToolType;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.IFluidTank;

import com.immersivereactors.immersivenuclear.common.INTileTypes;
import com.immersivereactors.immersivenuclear.common.Block.multiblock.INMultiblocks;
import com.immersivereactors.immersivenuclear.common.Block.multiblock.MetalMultiblock;
import com.immersivereactors.immersivenuclear.common.Block.metal.BWRTileentity;

public class BWRBlock extends MetalMultiblock<BWRTileentity>{
	//private static final Material material = new Material(MaterialColor.IRON, false, false, true, true, false, false, PushReaction.BLOCK);
	private static final Logger LOGGER = LogManager.getLogger();
	public BWRBlock() {
		
		super("bwreactor",()->INTileTypes.BWR.get());
		
		LOGGER.log(Level.DEBUG,"Thing-877756 debug msg");
		// TODO Auto-generated constructor stub
	}
	
}
