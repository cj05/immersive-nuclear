package com.immersivereactors.immersivenuclear.common.Block.multiblock;

import java.util.Iterator;
import java.util.function.Supplier;

import com.immersivereactors.immersivenuclear.ImmersiveNuclear;
import com.immersivereactors.immersivenuclear.common.INContent;
import com.immersivereactors.immersivenuclear.common.Items.CustomItemgroup;

import blusunrize.immersiveengineering.common.IEContent;
import blusunrize.immersiveengineering.common.blocks.BlockItemIE;
import blusunrize.immersiveengineering.common.blocks.generic.MultiblockPartTileEntity;
import blusunrize.immersiveengineering.common.blocks.metal.MetalMultiblockBlock;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.tileentity.TileEntityType;
import net.minecraftforge.fml.loading.FMLLoader;

public class MetalMultiblock<T extends MultiblockPartTileEntity<T>> extends MetalMultiblockBlock<T> {

	public MetalMultiblock(String name, Supplier<TileEntityType<T>> te) {
		super(name, te);
		
		if(!FMLLoader.isProduction()){
			IEContent.registeredIEBlocks.remove(this);
			Iterator<Item> it = IEContent.registeredIEItems.iterator();
			while(it.hasNext()){//why do you need this thing, didnt you remove it already
				
				Item item = it.next();
				if(item instanceof BlockItemIE && ((BlockItemIE) item).getBlock() == this){
					it.remove();
					break;
				}
			}
		}
		
		INContent.registeredBlocks.add(this);
		
		BlockItem bItem = new BlockItemIE(this, new Item.Properties().group(CustomItemgroup.INItemGroup));
		INContent.registeredItems.add(bItem.setRegistryName(getRegistryName()));
	}
	//copied from IP because i do not know how the fuck do i do it any other way
}
