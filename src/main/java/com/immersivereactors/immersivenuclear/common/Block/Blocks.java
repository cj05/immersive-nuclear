package com.immersivereactors.immersivenuclear.common.Block;

import com.google.common.base.Supplier;
import com.immersivereactors.immersivenuclear.ImmersiveNuclear;
import com.immersivereactors.immersivenuclear.common.Items.CustomItemgroup;
import com.immersivereactors.immersivenuclear.common.Items.Items;

import net.minecraft.block.AbstractBlock;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
//import net.minecraft.item.ItemGroup;
//import net.minecraft.block.Block;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;

public class Blocks {
	public static final DeferredRegister<Block> Blocks = 
			DeferredRegister.create(ForgeRegistries.BLOCKS, ImmersiveNuclear.MOD_ID);
	public static final class Multiblock{
		public static Block BWReactor;
	}
	public static final RegistryObject<Block> ControlRod = registerItemBlock("control_rod",()->new Block(AbstractBlock.Properties.create(Material.IRON)));
	public static final RegistryObject<Block> ReactorChamber = registerItemBlock("reactor_chamber",()->new Block(AbstractBlock.Properties.create(Material.IRON)));
	
	
	public static void register(IEventBus eventBus) {
		Blocks.register(eventBus);
	}
	static <T extends Block>RegistryObject<T> registerItemBlock(String name,Supplier<T> block) {
		RegistryObject<T> temporary = Blocks.register(name, block);
		Items.register(name,()->new BlockItem(temporary.get(),new Item.Properties().group(CustomItemgroup.INItemGroup)));
		return temporary;
	}
}
