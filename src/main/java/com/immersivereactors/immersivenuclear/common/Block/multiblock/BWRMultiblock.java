package com.immersivereactors.immersivenuclear.common.Block.multiblock;

import com.immersivereactors.immersivenuclear.ImmersiveNuclear;

//import java.util.function.Supplier;

import com.immersivereactors.immersivenuclear.common.Block.Blocks.Multiblock;
import com.mojang.blaze3d.matrix.MatrixStack;

import blusunrize.immersiveengineering.client.ClientUtils;
import blusunrize.immersiveengineering.common.blocks.multiblocks.IETemplateMultiblock;
//import net.minecraft.block.BlockState;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.model.ItemCameraTransforms.TransformType;
import net.minecraft.client.renderer.texture.OverlayTexture;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.vector.Quaternion;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.api.distmarker.Dist;

public class BWRMultiblock extends IETemplateMultiblock{
	
	public static final BWRMultiblock self = new BWRMultiblock();
	
	
	public BWRMultiblock() {
		super(new ResourceLocation(ImmersiveNuclear.MOD_ID,"multiblocks/bwreactor")
				, new BlockPos(2, 2, 4), new BlockPos(2, 2, 4), new BlockPos(5, 6, 5), 
				() -> Multiblock.BWReactor.getDefaultState());
		// TODO Auto-generated constructor stub
	}

	

	@Override
	@OnlyIn(Dist.CLIENT)
	public boolean canRenderFormedStructure() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public float getManualScale() {
		// TODO Auto-generated method stub
		return 4.0f;
	}
	
	@OnlyIn(Dist.CLIENT)
	private static ItemStack renderStack;
	
	@Override
	@OnlyIn(Dist.CLIENT)
	public void renderFormedStructure(MatrixStack transform, IRenderTypeBuffer buffer) {
		// TODO Auto-generated method stub
		if(renderStack==null)
			renderStack = new ItemStack(Multiblock.BWReactor);
		transform.translate(2.5, 2.25, 2.25);
		transform.rotate(new Quaternion(0, 45, 0, true));
		transform.rotate(new Quaternion(-20, 0, 0, true));
		transform.scale(6.5F, 6.5F, 6.5F);

		ClientUtils.mc().getItemRenderer().renderItem(
				renderStack,
				TransformType.GUI,
				0xf000f0,
				OverlayTexture.NO_OVERLAY,
				transform, buffer
		);
		
	}
	
}
