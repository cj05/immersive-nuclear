package com.immersivereactors.immersivenuclear.common;

import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;

import java.util.Collection;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.common.base.Supplier;
import com.google.common.collect.ImmutableSet;
import com.immersivereactors.immersivenuclear.ImmersiveNuclear;
import com.immersivereactors.immersivenuclear.common.Block.Blocks;
import com.immersivereactors.immersivenuclear.common.Block.metal.*;

import blusunrize.immersiveengineering.common.blocks.IEBlocks.Multiblocks;
import net.minecraft.block.Block;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityType;
//import net.minecraft.world.level.block;
public class INTileTypes {
	private static final Logger LOGGER = LogManager.getLogger();
	public static final DeferredRegister<TileEntityType<?>> REGISTER = DeferredRegister.create(
			ForgeRegistries.TILE_ENTITIES, ImmersiveNuclear.MOD_ID);
	
	
	public static final RegistryObject<TileEntityType<BWRTileentity>> BWR = register(
			"bwreactor", BWRTileentity::new ,Blocks.Multiblock.BWReactor
	);
	
	private static <T extends TileEntity> RegistryObject<TileEntityType<T>> register(String name, Supplier<T> factory, Block... valid){
		LOGGER.log(Level.DEBUG,"Registering Multiblock BWR....");
		TileEntityType<T> debug2= new TileEntityType<>(factory, ImmutableSet.copyOf(valid), null);
		RegistryObject<TileEntityType<T>> debug = REGISTER.register(name, () -> debug2);
		return debug;
	}
}
