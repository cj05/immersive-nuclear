package com.immersivereactors.immersivenuclear.common.Items;

import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;

public class CustomItemgroup {
	public static final ItemGroup INItemGroup = new ItemGroup("immersivenucleartab"){
		@Override
		public ItemStack createIcon() {
			return new ItemStack(Items.ZirconiumIngot.get());
		}
	};
}
