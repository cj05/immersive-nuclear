package com.immersivereactors.immersivenuclear.common.Items;


import com.google.common.base.Supplier;
import com.immersivereactors.immersivenuclear.ImmersiveNuclear;

//import net.minecraft.block.Block;
import net.minecraft.item.Item;
//import net.minecraft.item.ItemGroup;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
public class Items {
	
	public static final DeferredRegister<Item> ITEMS = 
			DeferredRegister.create(ForgeRegistries.ITEMS, ImmersiveNuclear.MOD_ID);
	
	public static final RegistryObject<Item> ZirconiumIngot = simple_register("zirconium_ingot");
	public static final RegistryObject<Item> ZirconiumNugget = simple_register("zirconium_nugget");
	
	public static void register(IEventBus eventBus) {
		ITEMS.register(eventBus);
	}
	public static RegistryObject<Item> simple_register(String name) {
		return register(name,
				()->new Item(new Item.Properties().group(CustomItemgroup.INItemGroup)));
	}
	public static <T extends Item>RegistryObject<T> register(String name,Supplier<T> block) {
		//ItemTag
		return ITEMS.register(name,block);
	}
}
